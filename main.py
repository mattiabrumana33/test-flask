from flask import Flask, request, render_template, url_for, session
from flaskext.mysql import MySQL

app = Flask(__name__)

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] =''
app.config['MYSQL_DATABASE_DB'] = 'rent_a_car'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

@app.route("/")
def home():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM car")
    data = cursor.fetchone()
    return render_template('index.html')

@app.route("/signup")
def SignUp():
    return render_template('signup.html')

if __name__ == "__main__":
    app.run(debug=True)
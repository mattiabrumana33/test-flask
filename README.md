# STUDENTS
+ 808374 Brumana Mattia
+ 803192 Carubelli Andrea


Link to the repository on GitLab:
+ [https://gitlab.com/andrea_caru/2019_assignment1_carubellibrumana/](https://gitlab.com/andrea_caru/2019_assignment1_carubellibrumana/)

# APPLICATION
An application that allows users to rent cars for a limited period of time.

The database consists of three tables: 
+ Car: a list of cars that can be rented;
+ Rental: a list of all the rentals with their starting date and ending date.
+ Client: a list of users that can rent a car.

The database used is a MySQL database, and it interfaces with the Python application via the "mysql.connector" library.

The user can login with its Postal Code and rent a car by entering the car license plate.

# DEVOPS
## Containerization/Virtualization
Use of two containers with two docker images:
+ Database: docker image mySQL:5.7
+ Application: docker image python:3.7

## Continuous Integration and Continuous Deployment CI/CD
The GitLab pipeline for the CI/CD is divided into 5 stages:
+ *build*: we combine the source code and its dependencies to build a runnable instance of our application. This stage of the CI/CD pipeline builds the Docker containers;
+ *verify*: stage that validates the code. We use Pyflakes, a simple program that analyzes programs and detects various errors by parsing the source file;
+ *unit-test*: we run automated tests to validate the correctness of our code and the behaviour of the application;
+ *release*: Docker images are renamed with the tag: *latest* and uploaded to the registry;
+ *deploy*:

### Build stage
+ *docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com*
+ *docker build -t $CONTAINER_IMAGE .*
+ *docker push $CONTAINER_IMAGE*

### Verify Stage
+ *pyflakes application/app.py*
+ *pyflakes application/database_functions.py*
+ *pyflakes application/test.py*

### Unit-Test stage
+ *python application/test.py*

### Release stage
+ *docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com*
+ *docker pull $CONTAINER_IMAGE*
+ *docker tag $CONTAINER_IMAGE $CONTAINER_RELEASE_IMAGE*
+ *docker push $CONTAINER_RELEASE_IMAGE*


Each stage is executed at each commit through the configuration file *.gitlab-ci.yml*.
# Use an official Python runtime as a parent image
FROM python:3.7

RUN git clone https://github.com/requests/requests.git \
        cd requests; \
        pip install -U pip; \
        pip install .;

RUN mkdir /flask
WORKDIR /flask

COPY requirements.txt .

RUN pip install -r requirements.txt

# Copy the current directory contents into the container at /application
COPY . .

CMD ["python", "main.py"]


import database_functions

def test_check_fc():
    fiscalCodes = ['CRBNDR96D18I577C','crbndr96d18i577c','CRBNDR96D18I577CC','BRMMTT96D03A794A','BR7MTT96D03A794A']
    is_valid = [True,False,False,True,False]
    for fiscalCode, valid in zip(fiscalCodes, is_valid):
        try:
            assert database_functions.check_fiscal_code(fiscalCode) == valid
        except AssertionError:
            print('Assertion fiscal code error')

def test_check_name():
    nomi = ['mario', 'marco%andrea' ,'marco-andrea','ele352onora','Giovanni','4232','Marco Apost']
    is_valid = [True,False,False,False,True,False,True]
    for nome, valid in zip(nomi, is_valid):
        try:
            assert database_functions.check_name(nome) == valid
        except AssertionError:
            print('Assertion name error, wrong string:')
            print(nome)

def test_check_surname():
    nomi = ['mario', 'marco%andrea' ,'marco-andrea','ele352onora','Giovanni','4232','Marco Apost']
    is_valid = [True,False,False,False,True,False,False]
    for nome, valid in zip(nomi, is_valid):
        try:
            assert database_functions.check_surname(nome) == valid
        except AssertionError:
            print('Assertion surname error')
            print(nome)

def test_check_place_of_birth():
    nomi = ['mario', 'marco%andrea' ,'marco-andrea','ele352onora','Giovanni','4232','Marco Apost']
    is_valid = [True,False,False,False,True,False,False]
    for nome, valid in zip(nomi, is_valid):
        try:
            assert database_functions.check_place_of_birth(nome) == valid
        except AssertionError:
            print('Assertion place of birth error')
            print(nome)

def test_check_date():
    dates = ['2019-12-10', '2019-12-32', '2019-30-12']
    is_valid = [True,False,False]
    for date, valid in zip(dates, is_valid):
        try:
            assert database_functions.check_date(date) == valid
        except AssertionError:
            print('Assertion date error')


